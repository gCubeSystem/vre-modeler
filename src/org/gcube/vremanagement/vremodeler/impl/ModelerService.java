package org.gcube.vremanagement.vremodeler.impl;

import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.gcube.common.core.faults.GCUBEFault;
import org.gcube.common.core.faults.GCUBEUnrecoverableFault;
import org.gcube.common.core.types.VOID;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.impl.deploy.DeployVRE;
import org.gcube.vremanagement.vremodeler.impl.util.Util;
import org.gcube.vremanagement.vremodeler.stubs.CollectionArray;
import org.gcube.vremanagement.vremodeler.stubs.CollectionList;
import org.gcube.vremanagement.vremodeler.stubs.CollectionType;
import org.gcube.vremanagement.vremodeler.stubs.FunctionalityIDArray;
import org.gcube.vremanagement.vremodeler.stubs.FunctionalityItem;
import org.gcube.vremanagement.vremodeler.stubs.FunctionalityList;
import org.gcube.vremanagement.vremodeler.stubs.GHNArray;
import org.gcube.vremanagement.vremodeler.stubs.GHNList;
import org.gcube.vremanagement.vremodeler.stubs.GHNType;
import org.gcube.vremanagement.vremodeler.stubs.Utils;
import org.gcube.vremanagement.vremodeler.stubs.VREDescription;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.State;
import org.globus.wsrf.ResourceException;

public class ModelerService {
	
	private static final GCUBELog logger = new GCUBELog(ModelerService.class);

	protected ModelerResource getResource() throws ResourceException{
		return (ModelerResource) ModelerContext.getPortTypeContext().getWSHome().find();
	}

	
	
	/**
	 * 
	 * @param request void
	 * @return VoidType
	 * @throws RemoteException -
	 *   
	 */	
	public void setDescription(VREDescription request) throws GCUBEFault{
		Date dateFrom=null, dateTo=null;
		ResultSet res=null;
		try {
			res=DBInterface.queryDB("select * from VRE where VRE.id='"+getResource().getId()+"'; ");
			dateFrom=new Date(request.getStartTime().getTimeInMillis());
			dateTo=new Date(request.getEndTime().getTimeInMillis());
			if(res.next())	
				DBInterface.queryDB("update VRE set name='"+request.getName()+"', description='"+request.getDescription()+"', vredesigner='"+request.getDesigner()+"', vremanager= '"+request.getManager()+"', intervalfrom='"+dateFrom+"', intervalto='"+dateTo+"' where VRE.id='"+getResource().getId()+"'; ");
			else{
				logger.debug("insert into VRE values('"+getResource().getId()+"','"+request.getName()+"','"+request.getDescription()+"','"+request.getDesigner()+"','"+request.getManager()+"','"+dateFrom+"','"+dateTo+"','"+State.Running+"'); ");
				DBInterface.ExecuteUpdate("insert into VRE values('"+getResource().getId()+"','"+request.getName()+"','"+request.getDescription()+"','"+request.getDesigner()+"','"+request.getManager()+"','"+dateFrom+"','"+dateTo+"','"+State.Running+"'); ");
			}
		} catch (Exception e) {
			logger.error("an error occurs setting the VRE Description",e);
			throw new GCUBEFault(e);
		}

	}



	/**
	 * 
	 * 
	 * @param request void
	 * @return VoidType
	 * @throws RemoteException -
	 */
	public VREDescription getDescription(VOID var) throws GCUBEFault{
		//logger.debug("Request DL with ID "+ID);
		try {
			DBInterface.connect();
			return Util.prepareVREDescription(DBInterface.queryDB("select * from VRE where VRE.id='"+getResource().getId()+"';"));
		} catch (Exception e) {
			logger.error("error getting VRE informations",e);
			throw new GCUBEFault(e);
		}
	}

	/**
	 * return an XML containing the GHN list
	 * 
	 * @param request void
	 * @return List of GHNs on the system
	 * @throws @throws GCUBEFault - if the clud is selected as Architecture or something fails on db
	 */
	public GHNList getGHNs(VOID var) throws GCUBEFault{
		logger.trace("getGHNs method");
		ResultSet res;
		ArrayList<String> relatedGHN=new ArrayList<String>();
		try{
			DBInterface.connect();
			res= DBInterface.queryDB("select * from GHN ORDER BY GHN.host;");

			ResultSet resRelated= DBInterface.queryDB("select ghnid, ISCANDIDATE from VRERELATEDGHN where VRERELATEDGHN.vreid='"+getResource().getId()+"';");
			
			String candidatedGHN="";
			while(resRelated.next()){
				relatedGHN.add(resRelated.getString(1));
				if (resRelated.getBoolean(2)) candidatedGHN=resRelated.getString(1);
			}
			
			return new GHNList(Util.prepareGHNs(res, relatedGHN,candidatedGHN).toArray(new GHNType[0]));
		}catch(Exception e) {
			logger.error("error retreiving GHNs",e);
			throw new GCUBEFault(e);}
	}


	/**
	 * Set the GHNs for deploying
	 *  
	 * @param request array of GHNs ids
	 * @return VoidType -
	 * @throws GCUBEFault - if the clud is selected as Architecture or something fails on db
	 */
	public void setGHNs(GHNArray request) throws GCUBEFault{
		try {
			if (getResource().isUseCloud()) throw new GCUBEFault("deploy on cloud is selected, the ghns cannot be set");
		} catch (ResourceException e1) {
			throw new GCUBEFault(e1);
		}
		try{
			String instanceID=(String) getResource().getId();
			DBInterface.ExecuteUpdate("DELETE FROM VRERELATEDGHN WHERE VRERELATEDGHN.vreid='"+instanceID+"';");
			
			String dlKey=instanceID;

			ArrayList<String[]> values= new ArrayList<String[]>(request.getGHNElement().length);
			ArrayList<String> row=null;
			for (String ghnId: request.getGHNElement())
			{
				row= new ArrayList<String>(3);
				row.add(dlKey);
				row.add(ghnId);
				if (request.getCandidateGHN()!=null && request.getCandidateGHN().compareTo(ghnId)==0) row.add("true");
				else row.add("false");
				values.add(row.toArray(new String[3]));
			}

			DBInterface.insertInto("VRERELATEDGHN", values.toArray(new String[0][0]));
		}catch(Exception e){
			logger.error("error setting GHNs ",e);
			throw new GCUBEFault(e);}
	}


	/**
	 * 
	 * Return an XML with all the collection selectable 
	 * 
	 * @param request void
	 * @return the collection retrieved from the database
	 * @throws GCUBEFault -
	 */
	public CollectionList getCollection(VOID var) throws GCUBEFault{
		ResultSet res;
		ArrayList<String> relatedCollection=new ArrayList<String>();
		try{
			DBInterface.connect();
			res= DBInterface.queryDB("select * from COLLECTION;");

						
			ResultSet resRelated= DBInterface.queryDB("select collid from VRERELATEDCOLLECTION where VRERELATEDCOLLECTION.vreid='"+getResource().getId()+"';");
			while(resRelated.next())
				relatedCollection.add(resRelated.getString(1));
			
		}catch(Exception e) {
			logger.error("error getting collections",e);
			throw new GCUBEFault(e);}

		
		return new CollectionList(Util.prepareCollections(res, relatedCollection).toArray(new CollectionType[0]));
	}

	/**
	 * 
	 * Insert in the DB the selected collection
	 * 
	 * @param request An Array that contains the selected collections ids 
	 * @return void
	 * @throws GCUBEFault -
	 */
	public void setCollection(CollectionArray request) throws GCUBEFault{
		try{
			logger.trace("method setCollection Called");
			DBInterface.connect();
			DBInterface.ExecuteUpdate("DELETE FROM VRERELATEDCOLLECTION WHERE VRERELATEDCOLLECTION.vreid='"+getResource().getId()+"';");

			String dlKey=(String) getResource().getId();

			ArrayList<String[]> values= new ArrayList<String[]>(request.getCollectionElement().length);
			ArrayList<String> row=null;
			
			logger.trace("the collection set for"+ dlKey+" are "+request.getCollectionElement().length);
			
			for (int i=0; i<request.getCollectionElement().length; i++)
			{
				row = new ArrayList<String>(2);
				row.add(dlKey);
				row.add(request.getCollectionElement()[i]);
				values.add(row.toArray(new String[2]));
			}
			DBInterface.connect();
			DBInterface.insertInto("VRERELATEDCOLLECTION", values.toArray(new String[0][0]));
		}catch(Exception e){
			logger.error("error setting collection",e);
			throw new GCUBEFault(e);
		}

	}

	/**
	 * Return an xml with the selectable functionalities
	 * String
	 * 
	 * @param request void
	 * @return an xml String containing selectable functionalities
	 * @throws GCUBEFault -
	 */
	public FunctionalityList getFunctionality(VOID var) throws GCUBEFault {

		ResultSet resFunct=null;
		ResultSet selectedFuncRes=null;
		ResultSet selectedCSRes=null;
		ArrayList<Integer> selectedFunctionality= new ArrayList<Integer>();
		ArrayList<String> selectedCS= new ArrayList<String>();

		try {
			DBInterface.connect();
			resFunct= DBInterface.queryDB("select * from functionality where father is null;");
			selectedFuncRes= DBInterface.queryDB("select funcid from VRERELATEDFUNCT where vreid='"+getResource().getId()+"';");
			while (selectedFuncRes.next()){
				selectedFunctionality.add(selectedFuncRes.getInt(1));
				logger.trace("selected functionality is "+selectedFuncRes.getInt(1));
			}
			selectedCSRes= DBInterface.queryDB("select csid from VRERELATEDCS where vreid='"+getResource().getId()+"';");
			while (selectedCSRes.next())
				selectedCS.add(selectedCSRes.getString(1));
		} catch (Exception e) {
			logger.error("VDLModel Service: error retrieving functionality",e);
			throw new GCUBEFault(e);
		}

		return new FunctionalityList(Util.prepareFunctionalities(resFunct, selectedFunctionality, selectedCS).toArray(new FunctionalityItem[0]));
	}


	/**
	 * 
	 * 
	 * 
	 * @return void
	 * @throws GCUBEFault -
	 */
	public void setFunctionality(FunctionalityIDArray request) throws GCUBEFault {
		logger.trace("Set Functionality called");
				
		try{
			DBInterface.ExecuteUpdate("DELETE FROM VRERELATEDFUNCT WHERE VRERELATEDFUNCT.vreid='"+getResource().getId()+"';");
			DBInterface.ExecuteUpdate("DELETE FROM VRERELATEDCS WHERE VRERELATEDCS.vreid='"+getResource().getId()+"';");
			

			String vreKey=(String) getResource().getId();
			//logger.debug("KEY: "+dlKey);
			
			if (request.getFunctionalityIDElement()!=null){
				ArrayList<String[]> values= new ArrayList<String[]>(request.getFunctionalityIDElement().length);
				ArrayList<String> row=null;
				for (String funct: request.getFunctionalityIDElement())
				{
					row= new ArrayList<String>(2);
					row.add(vreKey);
					row.add(funct);
					values.add(row.toArray(new String[2]));
				}

				DBInterface.insertInto("VRERELATEDFUNCT", values.toArray(new String[0][0]));
			}else logger.warn("requestFunciotnality is null");

			if (request.getCsIDElement()!=null)
			{	
				ArrayList<String[]> values= new ArrayList<String[]>(request.getCsIDElement().length);
				ArrayList<String> row=null;

				for (String css:request.getCsIDElement())
				{
					row= new ArrayList<String>(2);
					row.add(vreKey);
					row.add(css);
					values.add(row.toArray(new String[2]));
				}

				DBInterface.insertInto("VRERELATEDCS", values.toArray(new String[0][0]));
			}
		}catch(Exception e){
			logger.error("error setting functionality",e);
			throw new GCUBEFault(e);}

	}

	/**
	 * Return an XML with Quality parameter to insert
	 * 
	 * @param request void
	 * @return a XML format String
	 * @throws GCUBEFault -
	 */
	public String getQuality(VOID var) throws GCUBEFault{
		return Util.prepareQualityXML(); 
	}


	/**
	 * Sets Quality parameters
	 * 
	 * @param request
	 * @throws GCUBEFault -
	 */
	public void setQuality(String request) throws GCUBEFault{
		//TODO
	}

	/**
	 * 
	 * set the DL state to Pending
	 * 
	 * @param request
	 * @return
	 * @throws GCUBEFault -
	 */
	public void setVREtoPendingState(VOID var) throws GCUBEFault{
		try{
			DBInterface.ExecuteUpdate("UPDATE VRE SET STATUS='"+State.Pending+"' WHERE VRE.id='"+getResource().getId()+"';");
			
		}catch(Exception e){throw new GCUBEFault(e);}
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws GCUBEFault -
	 */
	public void deployVRE(VOID var) throws GCUBEFault{
		String resourceID; 
		ModelerResource resource;
		try {
			resource= getResource();
			resourceID = resource.getId();
		} catch (ResourceException e) {
			logger.error("Error retrieving the Resource Requested",e); throw new GCUBEUnrecoverableFault(e); 
		}
		
		try{
			DBInterface.ExecuteUpdate("UPDATE VRE SET STATUS='"+State.Running+"' WHERE VRE.id='"+resourceID+"';");
		} catch (Exception e) {
			logger.error("DB Error ",e);
			throw new GCUBEUnrecoverableFault(e);
		}				
		
		try{
			DeployVRE deployVREThread= new DeployVRE(resourceID, ServiceContext.getContext().getScope() );
			ServiceContext.getContext().setScope(deployVREThread, ServiceContext.getContext().getScope());		
			logger.trace("Deploy VRE thread started");
			deployVREThread.start();
		}catch (Exception e) {
			throw new GCUBEFault(e);
		}
	}

	/**
	 * 
	 * @param var
	 * @return
	 * @throws Exception
	 */
	public String checkStatus(VOID var) throws Exception{
		logger.trace("is deploy Report null?"+(getResource().getDeployReport()==null));
		return Utils.toXML(getResource().getDeployReport());
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws RemoteException -
	 */
	public void undeployVRE(VOID var) throws GCUBEFault{
		//TODO
	}
	
	
	/**
	 * 
	 * @param val boolean
	 */
	public void setUseCloud(boolean val) throws Exception{
		getResource().setUseCloud(val);
		getResource().store();
	}

	/**
	 * 
	 * @param r VOID
	 * @return
	 * @throws Exception
	 */
	public boolean isUseCloud(VOID r) throws Exception{
		return getResource().isUseCloud();
	}
	
	/**
	 * 
	 * @param numberOfVMs
	 * @throws Exception
	 */
	public void setCloudVMs(int numberOfVMs) throws Exception{
		ModelerResource resource = getResource();
		if (!resource.isUseCloud()) throw new Exception("the number of VMs cannot be set, you are not using cloud deployement");
		resource.setNumberOfVMsForCloud(numberOfVMs);
		resource.store();
	}
	
	/**
	 * 
	 * @param r VOID
	 * @return
	 * @throws Exception
	 */
	public int getCloudVMs(VOID r) throws Exception{
		ModelerResource resource = getResource();
		if (!resource.isUseCloud()) throw new Exception("the number of VMs cannot be returned, you are not using cloud deployement");
		return resource.getNumberOfVMsForCloud();
	}
	
}
