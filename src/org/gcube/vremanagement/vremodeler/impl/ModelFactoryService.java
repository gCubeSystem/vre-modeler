package org.gcube.vremanagement.vremodeler.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.axis.components.uuid.UUIDGen;
import org.apache.axis.components.uuid.UUIDGenFactory;
import org.apache.axis.message.addressing.EndpointReferenceType;
import org.gcube.common.core.faults.GCUBEFault;
import org.gcube.common.core.types.VOID;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.impl.util.Util;
import org.gcube.vremanagement.vremodeler.stubs.GetExistingNamesResponseMessage;
import org.gcube.vremanagement.vremodeler.stubs.Report;
import org.gcube.vremanagement.vremodeler.stubs.ReportList;


public class ModelFactoryService{
	
	private GCUBELog logger = new GCUBELog(ModelFactoryService.class);
	private static final UUIDGen uuidGen = UUIDGenFactory.getUUIDGen();
	
	protected ServiceContext getServiceContext() {
		return ServiceContext.getContext();
	}
	
	/**
	 * implementation of createResource method 
	 * @param request creation request	
	 * @return the EndpointReference pointing to the resource
	 * @throws GCUBEFault if something fails
	 */
	public EndpointReferenceType createResource() throws GCUBEFault {
		String id=uuidGen.nextUUID();
		logger.trace("resource "+id+" created");
		ModelerResource mr;
		try{
			ModelerContext pctx= ModelerContext.getPortTypeContext();
			mr=(ModelerResource)pctx.getWSHome().create(pctx.makeKey(id), id);
			mr.store();
			return mr.getEPR();
		}catch (Exception e){logger.error("error creating resource",e); throw new GCUBEFault(e);}
	}
	
	
	
	/**
	 * return the existing DL Name
	 * 
	 * @param request void
	 * @return array of string with existing VRE names
	 * @throws RemoteException -
	 */
	public GetExistingNamesResponseMessage getExistingNamesVREs(VOID arg) throws GCUBEFault {
		ArrayList<String> toReturn= new ArrayList<String>(); 
		ResultSet res=null;
		try{
			DBInterface.connect();
			res= DBInterface.queryDB("select VRE.name from VRE;");
			while (res.next())
				toReturn.add(res.getString(1));
		}catch(Exception e) {logger.error("error on DB",e); throw new GCUBEFault(e);}
		GetExistingNamesResponseMessage response = new GetExistingNamesResponseMessage();
		response.setNames(toReturn.toArray(new String[0]));
		
		return response;
	}
	
	
	
	
	/**
	 * Initialize the DB
	 * 
	 * @param request void
	 * @return void
	 * @throws GCUBEFault thrown if something fails 
	 */
	public void initDB(VOID arg) throws GCUBEFault {
		logger.debug("initDB method");
		
		Thread t= new Thread(){
			public void run(){
				try{
					ServiceContext.getContext().intializeDB();
				}catch(Exception e){
					logger.error("DB inizialization failed",e);
				}
			}
		};
			
		t.start();
	}
	
	
	/**
	 * 
	 * @param request void
	 * @return String
	 * @throws RemoteException -
	 */
	public ReportList getAllVREs(VOID arg)  throws GCUBEFault{
		ResultSet res=null;
		try{
			DBInterface.connect();
			res= DBInterface.queryDB("select VRE.name, VRE.description, VRE.status, VRE.id  from VRE;");
		}catch(Exception e) {logger.error("error on DB",e); throw new GCUBEFault(e);}
		try {
			return new ReportList(Util.prepareVREsList(res).toArray(new Report[0]));
		} catch (Exception e) {
			logger.error("parsing error in function getAllVREs ",e);
			throw new GCUBEFault(e);
		}
		
	}

		
	/**
	 * 
	 * remove the DL instance
	 * 
	 * @param request the id of VRE to remove
	 * @return void
	 * @throws RemoteException -
	 */
	public VOID removeVRE(String id) throws GCUBEFault{
		logger.trace("Deleting resource with id "+id);
		try{
			ModelerContext pctx= ModelerContext.getPortTypeContext();
			//destroy the resource;
			DBInterface.ExecuteUpdate("DELETE FROM VRE where VRE.id='"+id+"';");
			pctx.getWSHome().remove(pctx.makeKey(id));
			return new VOID();
		}catch(Exception e){
			logger.error("error removing resource",e);
			throw new GCUBEFault(e);
		}
		
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws GCUBEFault
	 */
	public EndpointReferenceType getEPRbyId(String id) throws GCUBEFault{
		ModelerContext pctx= ModelerContext.getPortTypeContext();
		try {
			return pctx.getWSHome().find(pctx.makeKey(id)).getEPR();
		} catch (Exception e) {
			logger.error("resource with id "+id+" not found",e);
			throw new GCUBEFault(e,"resource with id "+id+" not found");
		}
	}


	
	
}
