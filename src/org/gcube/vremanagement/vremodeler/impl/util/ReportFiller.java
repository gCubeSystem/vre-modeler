package org.gcube.vremanagement.vremodeler.impl.util;

import java.io.ByteArrayInputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.gcube.common.core.resources.GCUBECollection;
import org.gcube.common.core.resources.GCUBEGenericResource;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.DeployReport;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.FunctionalityDeployingReport;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.FunctionalityReport;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.Resource;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.ResourceDeployingReport;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.ServiceReport;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.State;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class ReportFiller {

	public static GCUBELog logger= new GCUBELog(ReportFiller.class);
	
	/**
	 * 
	 * @param report
	 * @param vreResourceId
	 * @throws Exception
	 */
	public static void initizlizeFunctionalityForReport(FunctionalityDeployingReport report, String vreResourceId) throws Exception{
		report.setState(State.Running);
		ResultSet resService =DBInterface.queryDB("select func.id, func.name, s.name, s.class, s.version from FUNCTIONALITY as func, VRERELATEDFUNCT AS vrf,  SERVICES AS s  where vrf.vreid='"+vreResourceId+"' AND s.id=vrf.funcid AND vrf.funcid=func.id;");
		while(resService.next()){
			FunctionalityReport funcReport= new FunctionalityReport();
			funcReport.setFunctionalityId(resService.getString(1));
			funcReport.setFunctionalityName(resService.getString(2));
			funcReport.setState(org.gcube.vremanagement.vremodeler.stubs.deployreport.State.Running);
			
			ServiceReport servReport= new ServiceReport();
			servReport.setServiceName(resService.getString(3));
			servReport.setServiceClass(resService.getString(4));
			servReport.setServiceVersion(resService.getString(5));
			servReport.setState(org.gcube.vremanagement.vremodeler.stubs.deployreport.State.Running);
			
			List<ServiceReport> listService;
			if (!report.getFunctionalityTable().containsKey(funcReport)){
				listService= new ArrayList<ServiceReport>();
				listService.add(servReport);
				report.getFunctionalityTable().put(funcReport, listService);
			}else 
				report.getFunctionalityTable().get(funcReport).add(servReport);
		}
	}
	
	/**
	 * 
	 * @param report
	 * @param vreResourceId
	 * @throws Exception
	 */
	public static void initializeResourcesForReport(ResourceDeployingReport report, String vreResourceId) throws Exception{
		report.setState(State.Running);
		ResultSet neededRes =DBInterface.queryDB("select n.id from NEEDEDRESOURCES AS n;");
		while (neededRes.next())
			report.getResources().add(new Resource(neededRes.getString(1), GCUBEGenericResource.TYPE));
		
		ResultSet resRelatedCol =DBInterface.queryDB("select VRERELATEDCOLLECTION.collid from VRERELATEDCOLLECTION where VRERELATEDCOLLECTION.vreid='"+vreResourceId+"';");
		while (resRelatedCol.next())
			report.getResources().add(new Resource(resRelatedCol.getString(1), GCUBECollection.TYPE));
		
	}
	
	/**
	 * 
	 * @param report
	 */
	public static void reportElaboration(DeployReport report){
		logger.trace("Elaborationg report");
		String rmReport = report.getFunctionalityDeployingReport().getResourceManagerReport();
		if (rmReport==null) return;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try{
			db = dbf.newDocumentBuilder();
			Document document = db.parse(new ByteArrayInputStream(rmReport.getBytes()));
			XPath xpath= XPathFactory.newInstance().newXPath();
			//first step: services retrieving
			for (Entry<FunctionalityReport,List<ServiceReport>> entry: report.getFunctionalityDeployingReport().getFunctionalityTable().entrySet()){
				State generalFunctState=State.Finished;
				for (ServiceReport serviceReport: entry.getValue()){
					if (serviceReport.getState()==State.Finished || serviceReport.getState()==State.Failed) continue;
					NodeList nResoulution = (NodeList)xpath.evaluate("/ResourceReport/Services/Service/DeploymentActivity/GHN/LastReportReceived/Packages/Package[/ServiceClass/text()='"+serviceReport.getServiceClass()+"' and /ServiceName/text()='"+serviceReport.getServiceName()+"' and /ServiceVersion/text()='"+serviceReport.getServiceVersion()+"']/Status",document,XPathConstants.NODESET);
					boolean isServiceDeployed=true;
					boolean isServiceFailed=false;
					for (int i = 0; i < nResoulution.getLength(); i++) {
						
						logger.trace("retrieved package for service "+serviceReport.getServiceName());
						
						if(nResoulution.item(i).getFirstChild().getNodeValue().compareTo("FAILED")==0){
							isServiceFailed=true;
							break;
						}else if(!(nResoulution.item(i).getFirstChild().getNodeValue().compareTo("RUNNING")==0 || nResoulution.item(i).getFirstChild().getNodeValue().compareTo("ACTIVATED")==0)){
							isServiceDeployed=false;
						}  
					}
					
					if (isServiceFailed){
						serviceReport.setState(State.Failed);
						generalFunctState= State.Failed;
					}
					if (isServiceDeployed) serviceReport.setState(State.Finished);
					else generalFunctState= State.Running;
				}
				entry.getKey().setState(generalFunctState);
			}
			
			logger.trace("second step : resources retrieving");
			
			//second step: resources retrieving
			//State generalResourceState=State.Finished;
			for (Resource resource:report.getResourceDeployingReport().getResources()){
				if (resource.getState()==State.Finished || resource.getState()==State.Failed) continue;
				logger.trace("checking resource with id "+resource.getResourceId());
				NodeList nResoulution = (NodeList)xpath.evaluate("/ResourceReport/Resources/Resource[/ID/text()='"+resource.getResourceId()+"']/Status",document,XPathConstants.NODESET); 
				logger.trace("found "+nResoulution.getLength());
				if (nResoulution.getLength()>0){
					logger.trace("found "+nResoulution.item(0).getFirstChild().getNodeValue());
					if(nResoulution.item(0).getFirstChild().getNodeValue().compareTo("FAILED")==0)
						resource.setState(State.Failed);
					else resource.setState(State.Finished);
				}
			}
		}catch (Exception e) {
			logger.warn("cannot fill report",e);
		}
		
		
	}
	
}
