package org.gcube.vremanagement.vremodeler.stubs.deployreport;

import java.io.Serializable;

public class FunctionalityReport implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1104477953274508866L;

	private State state;
	
	private String functionalityId;
	private String functionalityName;
	
	public FunctionalityReport(){
		this.state=State.Waiting;
	}
	
	public State getState() {
		return state;
	}



	public void setState(State state) {
		this.state = state;
	}



	public String getFunctionalityId() {
		return functionalityId;
	}



	public void setFunctionalityId(String functionalityId) {
		this.functionalityId = functionalityId;
	}



	public String getFunctionalityName() {
		return functionalityName;
	}



	public void setFunctionalityName(String functioanlityName) {
		this.functionalityName = functioanlityName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((functionalityId == null) ? 0 : functionalityId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FunctionalityReport other = (FunctionalityReport) obj;
		if (functionalityId == null) {
			if (other.functionalityId != null)
				return false;
		} else if (!functionalityId.equals(other.functionalityId))
			return false;
		return true;
	}

}