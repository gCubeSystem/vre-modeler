package org.gcube.vremanagement.vremodeler.stubs.deployreport;

import java.io.Serializable;

public class ResourceManagerDeployingReport implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8076338857724950353L;

	State state;
	
	private String resourceManagerReport;
	
	public ResourceManagerDeployingReport(){
		this.state= State.Waiting;
		this.resourceManagerReport=null;
	}
	
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getResourceManagerReport() {
		return resourceManagerReport;
	}

	public void setResourceManagerReport(String resourceManagerReport) {
		this.resourceManagerReport = resourceManagerReport;
	}
	
	
}
