package org.gcube.vremanagement.vremodeler.stubs.deployreport;

import java.io.Serializable;

public enum State implements Serializable {
	Running,
	Failed,
	Finished,
	Skipped,
	Pending,
	Waiting
}
