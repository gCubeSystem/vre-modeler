package org.gcube.vremanagement.vremodeler.stubs.deployreport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResourceDeployingReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1692792289962987059L;
	private State state;
	private List<Resource> resources;
	
	public ResourceDeployingReport(){
		this.resources=new ArrayList<Resource>();
	}
	
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}

	public List<Resource> getResources() {
		return resources;
	}

	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}
}
