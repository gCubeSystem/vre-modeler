package org.gcube.vremanagement.vremodeler.stubs.deployreport;

import java.io.Serializable;

/**
 * 
 * @author lucio
 *
 */
public class DeployReport implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7295105471156172674L;
	private State state;
	private GHNonCloudReport cloudDeployingReport;
	private ResourceManagerDeployingReport resourceManagerDeployingReport;
	private FunctionalityDeployingReport functionalityDeployingReport;
	private ResourceDeployingReport resourceDeployingReport;
	
	
	/**
	 * 
	 */
	public DeployReport(){
		this.state= State.Waiting;
		this.cloudDeployingReport= new GHNonCloudReport();
		this.cloudDeployingReport.setState(State.Waiting);
		this.functionalityDeployingReport= new FunctionalityDeployingReport();
		this.functionalityDeployingReport.setState(State.Waiting);
		this.resourceManagerDeployingReport= new ResourceManagerDeployingReport();
		this.resourceManagerDeployingReport.setState(State.Waiting);
		this.resourceDeployingReport= new ResourceDeployingReport();
		this.resourceDeployingReport.setState(State.Waiting);
	}
	
	/**
	 * 
	 * @return
	 */
	public State getState() {
		return state;
	}
	
	/**
	 * 
	 * @param state
	 */
	public void setState(State state) {
		this.state = state;
	}
	
	/**
	 * 
	 * @return
	 */
	public GHNonCloudReport getCloudDeployingReport() {
		return cloudDeployingReport;
	}
	
	/**
	 * 
	 * @param cloudDeployingReport
	 */
	public void setCloudDeployingReport(GHNonCloudReport cloudDeployingReport) {
		this.cloudDeployingReport = cloudDeployingReport;
	}
	
	/**
	 * 
	 * @return
	 */
	public ResourceManagerDeployingReport getResourceManagerDeployingReport() {
		return resourceManagerDeployingReport;
	}
	
	/**
	 * 
	 * @param resourceMaangerDeployingReport
	 */
	public void setResourceManagerDeployingReport(
			ResourceManagerDeployingReport resourceManagerDeployingReport) {
		this.resourceManagerDeployingReport = resourceManagerDeployingReport;
	}
	
	/**
	 * 
	 * @return
	 */
	public FunctionalityDeployingReport getFunctionalityDeployingReport() {
		return functionalityDeployingReport;
	}
	
	/**
	 * 
	 * @param functionalityDeployingReport
	 */
	public void setFunctionalityDeployingReport(
			FunctionalityDeployingReport functionalityDeployingReport) {
		this.functionalityDeployingReport = functionalityDeployingReport;
	}

	public ResourceDeployingReport getResourceDeployingReport() {
		return resourceDeployingReport;
	}

	public void setResourceDeployingReport(
			ResourceDeployingReport resourceDeployingReport) {
		this.resourceDeployingReport = resourceDeployingReport;
	}
}
