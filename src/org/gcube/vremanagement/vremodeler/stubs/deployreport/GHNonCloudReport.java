package org.gcube.vremanagement.vremodeler.stubs.deployreport;

import java.io.Serializable;

public class GHNonCloudReport implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1418965279876312220L;

	private State state;
	
	private State[] deployingState;

	public GHNonCloudReport() {
		this.state= State.Waiting;
	}
	
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public State[] getDeployingState() {
		return deployingState;
	}

	public void setDeployingState(State[] deployingState) {
		this.deployingState = deployingState;
	} 
}
