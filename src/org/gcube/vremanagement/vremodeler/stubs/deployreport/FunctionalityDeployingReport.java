package org.gcube.vremanagement.vremodeler.stubs.deployreport;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;

public class FunctionalityDeployingReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5667226186772008010L;

	private Hashtable<FunctionalityReport, List<ServiceReport>> functionalityTable= new Hashtable<FunctionalityReport, List<ServiceReport>>();
	
	private String resourceManagerReport;
	private State state;
	
	public FunctionalityDeployingReport(){
		this.state= State.Waiting;
		this.functionalityTable= new Hashtable<FunctionalityReport, List<ServiceReport>>();
	}
	
	
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Hashtable<FunctionalityReport, List<ServiceReport>> getFunctionalityTable() {
		return functionalityTable;
	}

	public void setFunctionalityTable(
			Hashtable<FunctionalityReport, List<ServiceReport>> functionalityTable) {
		this.functionalityTable = functionalityTable;
	}
	
	public String getResourceManagerReport() {
		return resourceManagerReport;
	}

	public void setResourceManagerReport(String resourceManagerReport) {
		this.resourceManagerReport = resourceManagerReport;
	}

}
