package org.gcube.vremanagement.vremodeler.stubs.deployreport;

import java.io.Serializable;

public class ServiceReport implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6061025649581168168L;

	private State state;
	
	private String serviceClass;
	private String serviceName;
	private String serviceVersion;
	
	public ServiceReport(){
		this.state= State.Waiting;
	}
	
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public String getServiceClass() {
		return serviceClass;
	}
	public void setServiceClass(String serviceClass) {
		this.serviceClass = serviceClass;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceVersion() {
		return serviceVersion;
	}
	public void setServiceVersion(String serviceVersion) {
		this.serviceVersion = serviceVersion;
	}
	
}