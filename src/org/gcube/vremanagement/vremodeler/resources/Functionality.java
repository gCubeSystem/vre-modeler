package org.gcube.vremanagement.vremodeler.resources;

import java.util.ArrayList;
import java.util.List;

public class Functionality {

	private String description;
	private String name;
	private List<Service> services= new ArrayList<Service>();
	private List<String> portlets= new ArrayList<String>();
	
	public List<Service> getServices() {
		return services;
	}
	public void setServices(List<Service> services) {
		this.services = services;
	}
	public List<String> getPortlets() {
		return portlets;
	}
	public void setPortlets(List<String> portlets) {
		this.portlets = portlets;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
