package org.gcube.vremanagement.vremodeler.resources;


public class GhnRiRelationObject {

	private String ghn;
	private String ri;
	
	public GhnRiRelationObject(String ghn, String ri) {
		super();
		this.ghn = ghn;
		this.ri = ri;
	}

	public String getGhn() {
		return ghn;
	}
	
	public String getRunningInstance() {
		return ri;
	}

	
	
	
	
}
