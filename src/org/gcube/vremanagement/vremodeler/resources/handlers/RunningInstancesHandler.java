package org.gcube.vremanagement.vremodeler.resources.handlers;

import java.util.ArrayList;
import java.util.List;
import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.informationsystem.client.ISClient;
import org.gcube.common.core.informationsystem.client.queries.GCUBERIQuery;
import org.gcube.common.core.resources.GCUBERunningInstance;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.impl.ServiceContext;
import org.gcube.vremanagement.vremodeler.resources.GhnRiRelationObject;

public class RunningInstancesHandler implements ResourceHandler<GCUBERunningInstance>{

	private static GCUBELog logger = new GCUBELog(RunningInstancesHandler.class);
	
	public static final String tableName="RunningInstance";
	//private static final String creationQuery="CREATE IF NOT EXISTS MEMORY TABLE "+tableName+" (ID VARCHAR NOT NULL PRIMARY KEY,NAME VARCHAR NOT NULL, CLASS VARCHAR NOT NULL)";
	
	private String relatedGhn;
	
	public RunningInstancesHandler(String ghnId) {
		 this.relatedGhn=ghnId;
	}
	
	public RunningInstancesHandler() {
		 this.relatedGhn=null;
	}
	
	
	public void initialize() throws Exception{
		ISClient client= GHNContext.getImplementation(ISClient.class);
		GCUBERIQuery queryRI= client.getQuery(GCUBERIQuery.class);
		queryRI.addGenericCondition("$result/Profile/GHN[string(@UniqueID) eq '"+this.relatedGhn+"']");
		List<GCUBERunningInstance> riList= client.execute(queryRI, ServiceContext.getContext().getScope());
		for (GCUBERunningInstance ri: riList)
			try{
				insert(ri);
			}catch(Exception e){logger.error("error insertin values in "+tableName, e);}	
	}
	
	
	
	private void insert(GCUBERunningInstance ri) throws Exception{
		ArrayList<String> row= new ArrayList<String>(3);
		row.add(ri.getID());
		row.add(ri.getServiceName());
		row.add(ri.getServiceClass());
		DBInterface.connect();
		DBInterface.insertInto(tableName, row.toArray(new String[0]));
		new GhnRiRelationHandler().add(new GhnRiRelationObject(this.relatedGhn, ri.getID()));
	}

	public void add(GCUBERunningInstance resource) throws Exception {
		this.insert(resource);
		
	}

	public void drop(String riId) throws Exception {
		DBInterface.connect();
		DBInterface.deleteElement(tableName, "ID='"+riId+"'");
		new GhnRiRelationHandler().drop(riId);
	}
	
}
