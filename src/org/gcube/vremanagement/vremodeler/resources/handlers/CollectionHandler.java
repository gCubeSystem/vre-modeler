package org.gcube.vremanagement.vremodeler.resources.handlers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.informationsystem.client.AtomicCondition;
import org.gcube.common.core.informationsystem.client.ISClient;
import org.gcube.common.core.informationsystem.client.queries.GCUBEGenericResourceQuery;
import org.gcube.common.core.resources.GCUBEGenericResource;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.impl.ServiceContext;

public class CollectionHandler implements ResourceHandler<GCUBEGenericResource> {

	private static GCUBELog logger = new GCUBELog(CollectionHandler.class);
	
	public static final String tableName="COLLECTION";
	
	public void add(GCUBEGenericResource resource) throws Exception {
		this.insert(resource);
		
	}

	public void drop(String collectionId) throws Exception {
		DBInterface.connect();
		DBInterface.deleteElement(tableName, "ID='"+collectionId+"'");
		
	}

	public void initialize() throws Exception {
		ISClient client= GHNContext.getImplementation(ISClient.class);
		//TODO: change when the new CM will return to GCUBECollection
		GCUBEGenericResourceQuery query=client.getQuery(GCUBEGenericResourceQuery.class);
		query.addAtomicConditions(new AtomicCondition("/Profile/Body/CollectionInfo/user","true"), new AtomicCondition("/Profile/SecondaryType", "GCUBECollection"));
		List<GCUBEGenericResource> collectionList= client.execute(query, ServiceContext.getContext().getScope());
		for (GCUBEGenericResource collection:collectionList)
			try{
				insert(collection);
			}catch(Exception e){logger.error("error inserting values in "+tableName, e);}
	}
	
	
	private void insert(GCUBEGenericResource collection) throws Exception {
		ArrayList<String> row= new ArrayList<String>(6);
		row.add(collection.getID());
		row.add(collection.getName());
		row.add(collection.getDescription()==null? "not provided" : collection.getDescription());
		row.add("-1");
		row.add(System.currentTimeMillis()+"");
		row.add(System.currentTimeMillis()+"");
		DBInterface.connect();
		DBInterface.insertInto(tableName, row.toArray(new String[6]));
	}

}
