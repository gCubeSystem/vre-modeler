package org.gcube.vremanagement.vremodeler.resources.handlers;

import java.util.ArrayList;
import java.util.List;
import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.informationsystem.client.AtomicCondition;
import org.gcube.common.core.informationsystem.client.ISClient;
import org.gcube.common.core.informationsystem.client.queries.GCUBEGHNQuery;
import org.gcube.common.core.resources.GCUBEHostingNode;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.impl.ServiceContext;

public class GHNHandler implements ResourceHandler<GCUBEHostingNode> {

	private static GCUBELog logger = new GCUBELog(GHNHandler.class);
	
	public static final String tableName="GHN";
	//private static final String creationQuery="CREATE MEMORY TABLE "+tableName+" (ID VARCHAR NOT NULL PRIMARY KEY,HOST VARCHAR,SECURITY VARCHAR,UPTIME VARCHAR,MAINMEMORYVA VARCHAR,MAINMEMORYVS VARCHAR,LOCALAS VARCHAR,LOCATION VARCHAR,COUNTRY VARCHAR,DOMAIN VARCHAR)";
		
	public void initialize() throws Exception{
		//DBInterface.connect();
		//DBInterface.ExecuteUpdate(creationQuery);
		ISClient client= GHNContext.getImplementation(ISClient.class);
		GCUBEGHNQuery query=client.getQuery(GCUBEGHNQuery.class);
		query.addAtomicConditions(new AtomicCondition("/Profile/GHNDescription/Type","Dynamic"));
		query.addAtomicConditions(new AtomicCondition("/Profile/GHNDescription/Status","certified"));
		List<GCUBEHostingNode> ghnList= client.execute(query, ServiceContext.getContext().getScope());
		for (GCUBEHostingNode ghn:ghnList)
			try{
				insert(ghn);
			}catch(Exception e){logger.error("error inserting values in "+tableName, e);}
	}
	
	private void insert(GCUBEHostingNode ghn) throws Exception {
		logger.trace("adding a GHN with id "+ghn.getID());
//GHN(ID VARCHAR NOT NULL PRIMARY KEY,HOST VARCHAR,SECURITY VARCHAR,UPTIME VARCHAR,MAINMEMORYVA VARCHAR,MAINMEMORYVS VARCHAR,LOCALAS VARCHAR,LOCATION VARCHAR,COUNTRY VARCHAR,DOMAIN VARCHAR, ISONCLOUD BOOLEAN)		
		ArrayList<String> row= new ArrayList<String>(10);
		String id= ghn.getID();
		row.add(id);
		row.add(ghn.getNodeDescription().getName());
		row.add(ghn.getNodeDescription().isSecurityEnabled()+"");
		row.add(ghn.getNodeDescription().getUptime());
		row.add(ghn.getNodeDescription().getMemory().getVirtualAvailable()+"");
		row.add(ghn.getNodeDescription().getMemory().getVirtualSize()+"");
		row.add(ghn.getNodeDescription().getLocalAvailableSpace()+"");
		row.add(ghn.getSite().getLocation());
		row.add(ghn.getSite().getCountry());
		row.add(ghn.getSite().getDomain());
		row.add("false");
		DBInterface.connect();
		DBInterface.insertInto(tableName, row.toArray(new String[0]));
		RunningInstancesHandler riHandler= new RunningInstancesHandler(ghn.getID());
		riHandler.initialize();
	}

	public void add(GCUBEHostingNode resource) throws Exception {
		this.insert(resource);
		
	}

	public void drop(String ghnId) throws Exception {
		logger.trace("removing a GHN with id "+ghnId);
		DBInterface.connect();
		DBInterface.deleteElement(tableName, "ID='"+ghnId+"'");
	}
	
}
