package org.gcube.vremanagement.vremodeler.resources.handlers;

import java.util.ArrayList;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.resources.GhnRiRelationObject;

public class GhnRiRelationHandler implements ResourceHandler<GhnRiRelationObject>{

	public static final String tableName="GHNRELATEDRI";
	//private static final String creationQuery="CREATE IF NOT EXISTS MEMORY TABLE "+tableName+"(GHNID VARCHAR NOT NULL,RIID VARCHAR NOT NULL,PRIMARY KEY(GHNID,RIID))";
	
	public void add(GhnRiRelationObject resource) throws Exception {
		ArrayList<String> row= new ArrayList<String>();
		row.add(resource.getGhn());
		row.add(resource.getRunningInstance());
		DBInterface.connect();
		DBInterface.insertInto(tableName, row.toArray(new String[0]));
	}

	public void drop(String riId) throws Exception {
		DBInterface.connect();
		DBInterface.deleteElement(tableName, "RIID='"+riId+"'");
	}

	public void initialize() throws Exception {}

}
