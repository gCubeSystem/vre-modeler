package org.gcube.vremanagement.vremodeler.resources.handlers;

import java.io.StringReader;
import java.sql.PreparedStatement;
import java.sql.Types;

import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.informationsystem.client.AtomicCondition;
import org.gcube.common.core.informationsystem.client.ISClient;
import org.gcube.common.core.informationsystem.client.queries.GCUBEGenericQuery;
import org.gcube.common.core.informationsystem.client.queries.GCUBEGenericResourceQuery;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.impl.ServiceContext;
import org.gcube.vremanagement.vremodeler.resources.Functionality;
import org.gcube.vremanagement.vremodeler.resources.MainFunctionality;
import org.gcube.vremanagement.vremodeler.resources.Service;
import org.gcube.vremanagement.vremodeler.resources.kxml.KGCUBEGenericFunctionalityResource;

public class FunctionalityHandler implements ResourceHandler<KGCUBEGenericFunctionalityResource>{
		
	private static GCUBELog logger= new GCUBELog(FunctionalityHandler.class);
	
	public static final String functionalityTableName="FUNCTIONALITY";
	public static final String portletTableName="PORTLETRELTOFUNCT";
	public static final String serviceTableName="SERVICES";
	
	private PreparedStatement psFunctionality;
	private PreparedStatement psServices;
	private PreparedStatement psPortlets;
	
	private ISClient queryClient;
		
	private String functionalityResourceId;
	
	public FunctionalityHandler() throws Exception{
		DBInterface.connect();
		this.psFunctionality=DBInterface.getConnection().prepareStatement("INSERT INTO FUNCTIONALITY VALUES(?,?,?,?,?)");
		this.psServices=DBInterface.getConnection().prepareStatement("INSERT INTO SERVICES VALUES(?,?,?,?)");
		this.psPortlets=DBInterface.getConnection().prepareStatement("INSERT INTO PORTLETRELTOFUNCT VALUES(?,?)");
		queryClient= GHNContext.getImplementation(ISClient.class);
	}

	
	
	public void add(KGCUBEGenericFunctionalityResource resource)
			throws Exception {
		DBInterface.deleteAll(serviceTableName);
		DBInterface.deleteAll(portletTableName);
		DBInterface.deleteAll(functionalityTableName);
		logger.debug("functionality clean");
		this.insert(resource);
		logger.debug("functionality changed");
	}

	public void drop(String resourceId)
			throws Exception {}

	public void initialize() throws Exception {
		
		KGCUBEGenericFunctionalityResource resource= new KGCUBEGenericFunctionalityResource();
		try{
			GCUBEGenericResourceQuery query= queryClient.getQuery(GCUBEGenericResourceQuery.class);
			query.addAtomicConditions(new AtomicCondition("/Profile/Name","FuctionalitiesResource"), new AtomicCondition("/Profile/SecondaryType","VREModelerResource"));
			GCUBEGenericQuery genericQuery= queryClient.getQuery(GCUBEGenericQuery.class);
			genericQuery.setExpression(query.getExpression());
			resource.load(new StringReader(queryClient.execute(genericQuery, ServiceContext.getContext().getScope()).get(0).toString()));
			this.functionalityResourceId= resource.getID();
			logger.debug("the functionality resource ID is "+this.functionalityResourceId);
			this.insert(resource);
			logger.debug("functionality initialized");
		}catch(Exception e ){logger.error("Functionality resource not found",e); logger.warn("the service will be initialized without functionalities"); }
		
	}
	
	private void insert( KGCUBEGenericFunctionalityResource resource) throws Exception{
		int id=0;
		for (MainFunctionality mainFunctionality: resource.getBody().getMainFunctionalities()){
			psFunctionality.setInt(1, id);
			psFunctionality.setString(2, mainFunctionality.getName());
			psFunctionality.setString(3,  mainFunctionality.getDescription());
			psFunctionality.setNull(4, Types.INTEGER);
			psFunctionality.setInt(5, 0);
			psFunctionality.execute();
			int rootFunctionalityId=id;
			id++;
			
			for (Functionality functionality: mainFunctionality.getFunctionalities()){
				psFunctionality.setInt(1, id);
				psFunctionality.setString(2, functionality.getName());
				psFunctionality.setString(3,  functionality.getDescription());
				psFunctionality.setInt(4, rootFunctionalityId);
				psFunctionality.setInt(5, 0);
				psFunctionality.execute();
				logger.trace("FUNCTIONALITY: "+functionality.getName()+" has "+functionality.getServices().size()+" services ");
				for (Service service: functionality.getServices()){
					psServices.setInt(1,id);
					psServices.setString(2, service.getServiceName());
					psServices.setString(3, service.getServiceClass());
					psServices.setString(4, "1.00.00");
					psServices.execute();
				}
				
				for (String portlet: functionality.getPortlets()){
					psPortlets.setInt(1, id);
					psPortlets.setString(2, portlet);
					psPortlets.execute();
				}
				id++;
			}
		}
	}



	public String getFunctionalityResourceId() {
		return functionalityResourceId;
	}

}
