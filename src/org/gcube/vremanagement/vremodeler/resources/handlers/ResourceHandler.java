package org.gcube.vremanagement.vremodeler.resources.handlers;

public interface ResourceHandler<T> {

	public void initialize() throws Exception;
	
	public void add(T resource) throws Exception;
	
	public void drop(String resourceId) throws Exception;
}
