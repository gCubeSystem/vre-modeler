package org.gcube.vremanagement.vremodeler.resources.handlers;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.informationsystem.client.ISClient;
import org.gcube.common.core.informationsystem.client.queries.GCUBEGenericResourceQuery;
import org.gcube.common.core.resources.GCUBEGenericResource;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.impl.ServiceContext;

public class GenericResourceHandler implements ResourceHandler<GCUBEGenericResource> {

	private static GCUBELog logger= new GCUBELog(GenericResourceHandler.class); 
	
	public static final String tableName="NEEDEDRESOURCES";
	
	public void add(GCUBEGenericResource resource) throws Exception {
		ArrayList<String> row= new ArrayList<String>(2);
		row.add(resource.getID());
		row.add(resource.getType());
		DBInterface.connect();
		DBInterface.insertInto(tableName, row.toArray(new String[0]));		
	}

	public void drop(String resourceId) throws Exception {
		DBInterface.connect();
		try{
			DBInterface.deleteElement(tableName, "ID='"+resourceId+"'");
		}catch(Exception e){logger.warn("the generic resource with id "+resourceId+" is not in the DB");}
	}

	public void initialize() throws Exception {
		StringBuilder temp= new StringBuilder();
		for (String secTypeResource:ServiceContext.getContext().getSecondaryTypeGenericResourceRequired())
			temp.append(" $result/Profile/SecondaryType/string() eq '"+secTypeResource.trim()+"' or");
		ISClient queryClient= GHNContext.getImplementation(ISClient.class);
		temp.delete(temp.length()-2,temp.length());
		logger.trace(temp);
		GCUBEGenericResourceQuery query= queryClient.getQuery(GCUBEGenericResourceQuery.class);
		query.addGenericCondition(temp.toString());
		List<GCUBEGenericResource> genericResourcesList= queryClient.execute(query, ServiceContext.getContext().getScope());
		for (GCUBEGenericResource genRes:genericResourcesList)
			this.add(genRes);
	}

}
