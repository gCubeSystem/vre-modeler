package org.gcube.vremanagement.vremodeler.test;

import java.util.Calendar;
import org.apache.axis.message.addressing.EndpointReferenceType;
import org.apache.axis.types.URI;
import org.gcube.common.core.contexts.GCUBERemotePortTypeContext;
import org.gcube.common.core.resources.GCUBEHostingNode.Site;
import org.gcube.common.core.scope.GCUBEScope;
import org.gcube.common.core.types.VOID;
import org.gcube.vremanagement.vremodeler.stubs.CollectionArray;
import org.gcube.vremanagement.vremodeler.stubs.CollectionList;
import org.gcube.vremanagement.vremodeler.stubs.FunctionalityIDArray;
import org.gcube.vremanagement.vremodeler.stubs.FunctionalityItem;
import org.gcube.vremanagement.vremodeler.stubs.FunctionalityList;
import org.gcube.vremanagement.vremodeler.stubs.GHNArray;
import org.gcube.vremanagement.vremodeler.stubs.ModelerFactoryPortType;
import org.gcube.vremanagement.vremodeler.stubs.ModelerServicePortType;
import org.gcube.vremanagement.vremodeler.stubs.Utils;
import org.gcube.vremanagement.vremodeler.stubs.VREDescription;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.DeployReport;
import org.gcube.vremanagement.vremodeler.stubs.deployreport.State;
import org.gcube.vremanagement.vremodeler.stubs.service.ModelerFactoryServiceAddressingLocator;
import org.gcube.vremanagement.vremodeler.stubs.service.ModelerServiceAddressingLocator;


public class ModelerTest {

	
	public static void main(String[] args) {
		try{
			/*
			ISClient client = GHNContext.getImplementation(ISClient.class);
			GCUBERIQuery riquery= client.getQuery(GCUBERIQuery.class);
			riquery.addAtomicConditions(new AtomicCondition("//ServiceName", "VREModeler"));
			List<GCUBERunningInstance> results=client.execute(riquery, GCUBEScope.getScope(args[0]));
			*/
			ModelerFactoryServiceAddressingLocator mfal =new ModelerFactoryServiceAddressingLocator();
			/*EndpointReferenceType epr= results.get(0).getAccessPoint().getEndpoint("gcube/vremanagement/vremodeler/ModelerFactoryService");
			System.out.println(epr);*/
			EndpointReferenceType epr= new EndpointReferenceType(new URI("http://nb-lelii.isti.cnr.it:8080/wsrf/services/gcube/vremanagement/vremodeler/ModelerFactoryService"));
			ModelerFactoryPortType mfptp= mfal.getModelerFactoryPortTypePort(epr);
			
			
			
			mfptp = GCUBERemotePortTypeContext.getProxy(mfptp, GCUBEScope.getScope(args[0]));
			
			EndpointReferenceType eprModelerRes= mfptp.createResource(new VOID());
			ModelerServiceAddressingLocator msal= new ModelerServiceAddressingLocator();
			ModelerServicePortType msptp=msal.getModelerServicePortTypePort(eprModelerRes);
			
			/*
			EndpointReferenceType eprModelerRes= mfptp.getEPRbyId("c85ef4c0-f178-11df-9ffd-b8524462ccae");
			
			ModelerServiceAddressingLocator msal= new ModelerServiceAddressingLocator();
			ModelerServicePortType msptp=msal.getModelerServicePortTypePort(eprModelerRes);
			msptp = GCUBERemotePortTypeContext.getProxy(msptp, GCUBEScope.getScope(args[0]));
			
			System.out.println(msptp.isUseCloud(new VOID()));
			
			System.out.println(msptp.getCloudVMs(new VOID()));
			
			System.out.println(msptp.checkStatus(new VOID()));
			
			DeployReport dr=Utils.fromXML(msptp.checkStatus(new VOID()));
			
			System.out.println(dr.getState());
			
			//System.out.println(msptp.checkStatus(new VOID()));
			
			*/
			
			msptp = GCUBERemotePortTypeContext.getProxy(msptp, GCUBEScope.getScope(args[0]));
			
			System.out.println("creation requested");
			
			
			
			VREDescription vreReq= new VREDescription();
			Calendar cal= Calendar.getInstance();
			vreReq.setStartTime(Calendar.getInstance());
			cal.add(Calendar.MONTH, 1);
			vreReq.setEndTime(cal);
			vreReq.setDescription("desc");
			vreReq.setDesigner("Lucio");
			vreReq.setManager("Lucio");
			vreReq.setName("ManzosCloudVRE");
			msptp.setDescription(vreReq);
			System.out.println("description set");
			
			CollectionList cl=msptp.getCollection(new VOID());
		
			System.out.println("collection list is null?"+(cl.getList()==null));
			
			for(int i=0; i< cl.getList().length; i++)
				System.out.println(i+" - "+cl.getList(i));
			
			msptp.setCollection(new CollectionArray(new String[]{cl.getList(0).getId()}));
		
			FunctionalityList flist=msptp.getFunctionality(new VOID());
			
			for (FunctionalityItem fitem: flist.getList()){
				for (FunctionalityItem fchild:fitem.getChilds())
					System.out.println(fchild.getName()+" "+fchild.getId());
			}
			
			System.out.println("collection set");
			
			
			FunctionalityIDArray fida= new FunctionalityIDArray();
			
			
			fida.setFunctionalityIDElement(new String[]{"4"});
			msptp.setFunctionality(fida);

			System.out.println("functionality set");
			
			msptp.setUseCloud(true);
			msptp.setCloudVMs(2);
			
			/*
			msptp.setUseCloud(false);
			msptp.setGHNs(new GHNArray(new String[]{"038bab20-eccd-11df-890f-bfe609d68cc0","046da6b0-eccd-11df-90ca-ecd7051b065f"},"038bab20-eccd-11df-890f-bfe609d68cc0"));
			*/
			
			msptp.deployVRE(new VOID());
			DeployReport deployReport;
			do{
				String report=msptp.checkStatus(new VOID());
				System.out.println("report is:");
				System.out.println(report);
				deployReport= Utils.fromXML(report);
				Thread.sleep(15000);
			}while (deployReport.getState()==State.Running);
			
		}catch(Exception e){e.printStackTrace();}
		
	}
	
}
