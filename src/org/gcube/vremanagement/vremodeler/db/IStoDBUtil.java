package org.gcube.vremanagement.vremodeler.db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.SQLException;
import org.gcube.common.core.faults.GCUBEFault;
import org.gcube.common.core.scope.GCUBEScope;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.impl.ServiceContext;
import org.gcube.vremanagement.vremodeler.resources.handlers.CollectionHandler;
import org.gcube.vremanagement.vremodeler.resources.handlers.FunctionalityHandler;
import org.gcube.vremanagement.vremodeler.resources.handlers.GHNHandler;
import org.gcube.vremanagement.vremodeler.resources.handlers.GenericResourceHandler;
import org.gcube.vremanagement.vremodeler.resources.handlers.GhnRiRelationHandler;
import org.gcube.vremanagement.vremodeler.resources.handlers.RunningInstancesHandler;



/**
 * 
 * @author lucio lelii CNR
 *
 */
public class IStoDBUtil {

	private static GCUBELog logger = new GCUBELog(IStoDBUtil.class);
			
	private static void createTables(GCUBEScope scope) throws GCUBEFault{
		try{
			DBInterface.connect();
			File file= ServiceContext.getContext().getFile("hsqldb/vreModelerDBInitializer", false);
			FileReader fileReader= new FileReader(file);
			BufferedReader in = new BufferedReader(fileReader);
			String line;
			while ((line=in.readLine())!=null){
				logger.trace(line);
				DBInterface.ExecuteUpdate(line);
			}
		}catch (Exception e){
			logger.error("error creating tables",e); 
			throw new GCUBEFault(e); }
	}
	
	/**
	 * Initialize all database tables
	 * 
	 * @param GCUBEScope  the scope
	 */
	public static void initDB(GCUBEScope scope) throws GCUBEFault{
		
		logger.info("Starting initialization!!");
		if (!DBInterface.dbAlreadyCreated())
			createTables(scope);
		else cleanDB(scope);
	}
	
	private static void cleanDB(GCUBEScope scope) throws GCUBEFault
	{
		try{	
			DBInterface.connect();
			//DBInterface.deleteAll("VRERELATEDCOLLECTION");
			//DBInterface.deleteAll("VRERELATEDCS");
			//DBInterface.deleteAll("VRERELATEDFUNCT");
			//DBInterface.deleteAll("VRERELATEDGHN");
			//DBInterface.deleteAll("VRE");
			//DBInterface.deleteAll(NativeMetadataFormatHandler.tableName);
			//DBInterface.deleteAll("derivablemdf");
			//DBInterface.deleteAll(MetadataFormatHandler.tableName);
			DBInterface.deleteAll(CollectionHandler.tableName);
			//DBInterface.deleteAll("relatedserviceid");
			DBInterface.deleteAll(GHNHandler.tableName);
			DBInterface.deleteAll(RunningInstancesHandler.tableName);
			DBInterface.deleteAll(GhnRiRelationHandler.tableName);
			//DBInterface.deleteAll("cs");
			DBInterface.deleteAll(GenericResourceHandler.tableName);
			DBInterface.deleteAll(FunctionalityHandler.portletTableName);
			DBInterface.deleteAll(FunctionalityHandler.serviceTableName);
			DBInterface.deleteAll(FunctionalityHandler.functionalityTableName);
		}catch (SQLException e){
			logger.error("error cleaning sqlDB",e); 
			throw new GCUBEFault(e); }
		
	}
	
	
	/*
	private static void insertNeededResources(GCUBEScope scope) throws GCUBEFault{
		List<GCUBEGenericResource> genericResourcesList= null;
		try{
			if (queryClient==null) queryClient= GHNContext.getImplementation(ISClient.class);
			GCUBEGenericResourceQuery query= queryClient.getQuery(GCUBEGenericResourceQuery.class);
			query.addGenericCondition("$result/Profile/SecondaryType/string() eq 'UserProfile' or $result/Profile/SecondaryType/string() eq 'Schemas Searchable Fields' or $result/Profile/SecondaryType/string() eq 'MetadataXSLT' or $result/Profile/SecondaryType/string() eq 'PresentationXSLT'");
			genericResourcesList= queryClient.execute(query, scope);
		}catch(Exception e ){logger.error("Error queryng IS"); throw new GCUBEFault(e); }
		
		
		//NEEDEDRESOURCES(ID, TYPE)
		List<String[]> values = new ArrayList<String[]>(genericResourcesList.size());
		List<String> row; 
		for (GCUBEGenericResource gen :genericResourcesList){
			row= new ArrayList<String>(2);
			row.add(gen.getID());
			row.add(gen.getType());
			values.add(row.toArray(new String[2]));
		}
		
		try{
			DBInterface.connect();
			DBInterface.insertInto("NEEDEDRESOURCES", values.toArray(new String[0][0]));
		}catch (SQLException e){
			logger.error("error inserting Generic resource in the DB ",e); 
			throw new GCUBEFault(e); 
		}
	
	}
	*/
	
}
