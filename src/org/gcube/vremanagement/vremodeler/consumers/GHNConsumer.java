package org.gcube.vremanagement.vremodeler.consumers;

import java.io.StringReader;
import java.sql.ResultSet;

import javax.xml.namespace.QName;

import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.contexts.GHNContext.Status;
import org.gcube.common.core.informationsystem.notifier.ISNotifier.BaseNotificationConsumer;
import org.gcube.common.core.informationsystem.notifier.ISNotifier.GCUBENotificationTopic;
import org.gcube.common.core.informationsystem.notifier.ISNotifier.NotificationEvent;
import org.gcube.common.core.resources.GCUBEHostingNode;
import org.gcube.common.core.scope.GCUBEScope;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.db.DBInterface;
import org.gcube.vremanagement.vremodeler.impl.ServiceContext;
import org.gcube.vremanagement.vremodeler.resources.handlers.GHNHandler;


public class GHNConsumer extends BaseNotificationConsumer{
	
	public static GCUBENotificationTopic  ghnTopic;
	
	static{
		ghnTopic= new GCUBENotificationTopic(new QName("http://gcube-system.org/namespaces/informationsystem/registry","GHN"));
		ghnTopic.setPrecondition("(//operationType[text()='destroy']) or (//operationType[text()='update'])");
		ghnTopic.setUseRenotifier(false);
	}
	
	private GCUBELog logger= new GCUBELog(GHNConsumer.class);
		
	 
		
	private GCUBEScope scope;
	
	public GHNConsumer(GCUBEScope scope){
		super();
		this.scope=scope;
	}
	
	public void onNotificationReceived(NotificationEvent event){
		try{
			//logger.trace("notification received");
			ServiceContext.getContext().setScope(this.scope);
			String id= event.getPayload().getMessage()[0].getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
			String operation=event.getPayload().getMessage()[0].getChildNodes().item(2).getChildNodes().item(0).getNodeValue();
			//logger.trace("received id: "+id+" op: "+operation+" profile: "+profile);
			
			if (operation.compareTo("update")==0){
				//logger.trace("adding a new GHN in DB");
				GCUBEHostingNode ghn= GHNContext.getImplementation(GCUBEHostingNode.class);
				String profile=event.getPayload().getMessage()[0].getChildNodes().item(1).getChildNodes().item(0).getNodeValue();
				ghn.load(new StringReader(profile));
				//if (ghn.getNodeDescription().getStatus()!=Status.CERTIFIED) return;
				ResultSet queryRes= DBInterface.queryDB("select host from ghn where id='"+ghn.getID()+"'");
				if (queryRes.next()){
					if (ghn.getNodeDescription().getStatus()!=Status.CERTIFIED)
						new GHNHandler().drop(ghn.getID());
				} else {
					if (ghn.getNodeDescription().getStatus()==Status.CERTIFIED)
						new GHNHandler().add(ghn);
				}
			} else if (operation.compareTo("destroy")==0){
				logger.trace("removing a GHN from DB");
				new GHNHandler().drop(id);
			}

		}catch(Exception e){logger.error("error in notification received",e);}
	}
}
