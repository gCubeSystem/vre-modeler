package org.gcube.vremanagement.vremodeler.consumers;

import java.io.StringReader;
import javax.xml.namespace.QName;
import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.informationsystem.notifier.ISNotifier.BaseNotificationConsumer;
import org.gcube.common.core.informationsystem.notifier.ISNotifier.GCUBENotificationTopic;
import org.gcube.common.core.informationsystem.notifier.ISNotifier.NotificationEvent;
import org.gcube.common.core.resources.GCUBEGenericResource;
import org.gcube.common.core.scope.GCUBEScope;
import org.gcube.common.core.utils.logging.GCUBELog;
import org.gcube.vremanagement.vremodeler.impl.ServiceContext;
import org.gcube.vremanagement.vremodeler.resources.handlers.FunctionalityHandler;
import org.gcube.vremanagement.vremodeler.resources.handlers.GenericResourceHandler;
import org.gcube.vremanagement.vremodeler.resources.kxml.KGCUBEGenericFunctionalityResource;

public class GenericResourceConsumer extends BaseNotificationConsumer{

private GCUBELog logger= new GCUBELog(GHNConsumer.class);
	
	public static final GCUBENotificationTopic  functionalityTopic= new GCUBENotificationTopic(new QName("http://gcube-system.org/namespaces/informationsystem/registry","GenericResource"));
		
	static{
		functionalityTopic.setUseRenotifier(false);
	}
	
	private String functionalityResourceId;
	private GCUBEScope scope;
	
	public GenericResourceConsumer(GCUBEScope scope, String resourceId){
		super();
		this.scope=scope;
		this.functionalityResourceId= resourceId;
	}
	
	public void onNotificationReceived(NotificationEvent event){
		try{
			ServiceContext.getContext().setScope(this.scope);
			String id= event.getPayload().getMessage()[0].getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
			String operation=event.getPayload().getMessage()[0].getChildNodes().item(2).getChildNodes().item(0).getNodeValue();
			
			logger.info("notification received for genericResource "+id+" and operation "+operation);
			
			if ((operation.compareTo("update")==0) && id.compareTo(this.functionalityResourceId)==0){
				logger.trace("notification received for functionalityResource with id "+id+" in scope "+scope.toString());
				KGCUBEGenericFunctionalityResource resource= new KGCUBEGenericFunctionalityResource();
				String profile=event.getPayload().getMessage()[0].getChildNodes().item(1).getChildNodes().item(0).getNodeValue();
				resource.load(new StringReader(profile));
				//FunctionalityHandler
			 	FunctionalityHandler functionalityHandler= new FunctionalityHandler();
			 	functionalityHandler.add(resource);
			}else if (operation.compareTo("create")==0){
				logger.trace("notification received for generic resource with operation create");
				
				GCUBEGenericResource genericResource= GHNContext.getImplementation(GCUBEGenericResource.class);
				String profile=event.getPayload().getMessage()[0].getChildNodes().item(1).getChildNodes().item(0).getNodeValue();
				genericResource.load(new StringReader(profile));
				if (genericResource.getName().compareTo("FuctionalitiesResource")==0){
					FunctionalityHandler functionalityHandler= new FunctionalityHandler();
					KGCUBEGenericFunctionalityResource functResource=new KGCUBEGenericFunctionalityResource();
					functResource.load(new StringReader(profile));
					this.functionalityResourceId=functResource.getID();
					functionalityHandler.add(functResource);
				} else
					if (ServiceContext.getContext().getSecondaryTypeGenericResourceRequired().contains(genericResource.getSecondaryType()))
						new GenericResourceHandler().add(genericResource);
			}else if (operation.compareTo("destroy")==0){
				logger.trace("notification received for generic resource with operation destroy");
				new GenericResourceHandler().drop(id);
			}

		}catch(Exception e){logger.error("error in functionality notification",e);}
	}
	
}
